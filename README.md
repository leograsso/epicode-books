# Epicode Books

Gestione del catalogo di una biblioteca

## Specifiche

La libreria gestisce nel catalogo libri e prodotti:

Per ogni libro � necessario gestire:
- Titolo
- Autori (Possono essere pi� di uno)
- Editore
- Data di pubblicazione
- Codice ISBN
- Codice interno della biblioteca
- Numero di copie a disposizione
- Volumi in cui � strutturato
- Lingua
- Categoria di classificazione

Ogni copia a disposizione ha:

- Una posizione nella biblioteca
- Una indicazione che comunica se disponibile per il prestito o meno
- Uno stato (In consultazione, in prestito, disponibile)

Ogni volume ha:
- Titolo
- Codice ISBN
- Codice interno
- Numero di pagine

Ogni autore � caratterizzato da:
- Nome
- Cognome
- Nazionalit�
- Anno di nascita
- Anno di morte (se non in vita)

I periodoci sono caratterizzati da:
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione (Anche pi� di una)
- Periodicit� (quotidiano, mensile, annuale, estemporaneo, ecc)
